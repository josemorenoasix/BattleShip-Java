package jmoreno.daw;

import jmoreno.daw.core.game.Game;

public class App {
    public static void main(String[] args) {

        Game game = new Game();
        game.startGame();

    }
}
