package jmoreno.daw.consoleui;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_LongestLine;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import jmoreno.daw.core.board.Board;
import jmoreno.daw.core.board.Tile;

import java.util.Scanner;
import java.util.regex.Pattern;

public class ConsoleUI {

    public static String scanHumanShotValue() {

        Scanner scan = new Scanner(System.in);

        final String regex = "^([A-J]|[a-j])[0-9]$";
        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);

        String value;

        System.out.print("Introduce coordenada (ej. A5): ");
        while (!scan.hasNext(pattern)) {
            System.out.println("Incorrecto!");
            System.out.print("Introduce coordenada (ej. A5): ");
            scan.next();
        }
        value = scan.next().toUpperCase();

        return value;

    }

    public void printWelcome() {
        System.out.println("\nWelcome to BattleShip!");
    }

    public void printGameBoard(Board board) {

        StringBuilder string = new StringBuilder("\n");

        string.append(Color.CYAN + "* " + Color.RESET);

        for (int i = 0; i < board.getBoardCols(); i++) {
            string.append(Color.CYAN).append((char) (i + 65)).append(" ").append(Color.RESET);
        }

        string.append("\n");

        for (int i = 0; i < board.getBoardCols(); i++) {
            string.append(Color.CYAN).append(i).append(" ").append(Color.RESET);
            for (int j = 0; j < board.getBoardRows(); j++) {
                string.append(printTile(board.getBoardTile(i, j)));
            }
            string.append("\n");
        }

        System.out.println(string);
    }

    public void printResult(boolean humanWinner) {
        if (humanWinner) {
            System.out.println("Has ganado!");
        } else {
            System.out.println("Has perdido!");
        }

    }

    //TODO Report Class
    public void printGameReport(
            int shots,
            boolean succesComputerShot,
            String coordinateComputerShot,
            boolean sunkedComputerShip,
            int sizeComputerNavy,
            boolean succesHumanShot,
            String coordinateHumanShot,
            boolean sunkedHumanShip,
            int sizeHumanNavy
    ) {

        AsciiTable at = new AsciiTable();

        at.addRule();
        at.addRow(shots, "Reporte", "Flota");
        at.addRule();
        at.addRow("Jugador", "(" + coordinateHumanShot + ")" + printShotResult(succesHumanShot, sunkedComputerShip), sizeHumanNavy);
        at.addRule();
        at.addRow("Enemigo", "(" + coordinateComputerShot + ")" + printShotResult(succesComputerShot, sunkedHumanShip), sizeComputerNavy);
        at.addRule();
        at.setTextAlignment(TextAlignment.CENTER);
        CWC_LongestLine cwc = new CWC_LongestLine();
        at.getRenderer().setCWC(cwc);
        System.out.println(at.render());

    }


    private String printTile(Tile tile) {

        if (tile == Tile.WATER) {
            return Color.BLUE + "~" + " " + Color.RESET;
        } else if (tile == Tile.SHIP) {
            return Color.YELLOW + "#" + " " + Color.RESET;
        } else if (tile == Tile.SUCCESS_SHOT) {
            return Color.RED + "#" + " " + Color.RESET;
        } else if (tile == Tile.FAIL_SHOT) {
            return Color.RED + "~" + " " + Color.RESET;
        } else {
            return null;
        }

    }

    private String printShotResult(boolean succesShot, boolean sunkedShip) {

        if (succesShot) {

            if (sunkedShip) {

                return "HUNDIDO";

            } else {

                return "TOCADO";

            }

        } else {

            return "AGUA";

        }

    }


}
