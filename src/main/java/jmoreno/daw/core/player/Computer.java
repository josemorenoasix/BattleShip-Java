package jmoreno.daw.core.player;

import jmoreno.daw.core.board.Board;

import java.util.Random;


public class Computer extends Player {

    private Random rnd;

    public Computer() {

        this.rnd = new Random();

    }

    @Override
    public int[] shotRandom(Board board) {

        int[] shot = new int[2];


        shot[0] = rnd.nextInt(10);
        shot[1] = rnd.nextInt(10);

        if (!board.isTileFailShot(shot[0], shot[1]) && !board.isTileSuccessShot(shot[0], shot[1])) {

            board.shot(shot[0], shot[1]);

        } else {

            shotRandom(board);

        }

        return shot;
    }

}
