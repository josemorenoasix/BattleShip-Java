package jmoreno.daw.core.player;

import jmoreno.daw.core.board.Board;

public class Human extends Player {

    @Override
    public void shot(Board board, int row, int col) {
        board.shot(row, col);
    }

    @Override
    public void markShot(Board auxBoard, Board board, int row, int col) {

        if (board.isTileSuccessShot(row, col) || board.isTileShip(row, col)) {
            auxBoard.markSuccessShot(row, col);
        } else {
            auxBoard.markFailShot(row, col);
        }

    }

}
