package jmoreno.daw.core.player;

import jmoreno.daw.core.board.Board;

public abstract class Player {

    /**
     * Perform a shot.
     *
     * @param board where shot will go.
     * @param row   coordinate on board.
     * @param col   coordinate on board.
     */
    public void shot(Board board, int row, int col) {

    }


    /**
     * Perform a random shot.
     *
     * @param board where shot will go.
     */
    public int[] shotRandom(Board board) {

        return new int[0];
    }


    /**
     * Mark the shot in a auxiliar game board.
     *
     * @param auxBoard where mark will go.
     * @param board    to check the result of a shot
     * @param row      coordinate on board.
     * @param col      coordinate on board.
     */
    public void markShot(Board auxBoard, Board board, int row, int col) {


    }
}
