package jmoreno.daw.core.game;

import jmoreno.daw.consoleui.ConsoleUI;
import jmoreno.daw.core.board.Board;
import jmoreno.daw.core.board.BoardFactory;
import jmoreno.daw.core.board.Ship;
import jmoreno.daw.core.player.Computer;
import jmoreno.daw.core.player.Human;
import jmoreno.daw.core.player.Player;

import java.util.ArrayList;

public class Game {

    private final int boardRowSize = 10;
    private final int boardColSize = 10;
    private final int[] shipSizes = {4, 3, 3, 2, 2, 2, 1, 1, 1, 1};
    private Player computer;
    private Player human;
    private Board computerBoard;
    private Board humanBoard;
    private Board humanAuxBoard;
    private ConsoleUI consoleUI;

    public Game() {

        this.computer = new Computer();
        this.human = new Human();
        this.computerBoard = new Board(boardRowSize, boardColSize);
        this.humanBoard = new Board(boardRowSize, boardColSize);
        this.humanAuxBoard = new Board(boardRowSize, boardColSize);
        this.consoleUI = new ConsoleUI();

    }

    public void startGame() {

        int shots = 0;

        consoleUI.printWelcome();

        BoardFactory boardFactory = new BoardFactory();

        boardFactory.randomize(humanBoard, shipSizes);
        boardFactory.randomize(computerBoard, shipSizes);

        //consoleUI.printGameBoard(computerBoard);
        consoleUI.printGameBoard(humanBoard);

        while (isGameNotFinish(computerBoard) && isGameNotFinish(humanBoard)) {

            int[] computerShot = computer.shotRandom(humanBoard);

            String response = ConsoleUI.scanHumanShotValue();
            int col = computerBoard.getCoordinatesMap().getCol(response);
            int row = computerBoard.getCoordinatesMap().getRow(response);

            human.shot(computerBoard, row, col);

            boolean isSuccessComputerShot = isSuccessComputerShot(computerShot[0], computerShot[1]);
            boolean isSuccessHumanShot = isSuccessHumanShot(row, col);

            if (isSuccessHumanShot) {
                humanAuxBoard.markSuccessShot(row, col);
            } else {
                humanAuxBoard.markFailShot(row, col);
            }

            boolean isSunkShipOnHumanBoard = checkSunkenShip(humanBoard);
            boolean isSunkShipOnComputerBoard = checkSunkenShip(computerBoard);

            if (isSunkShipOnHumanBoard) {
                removeSunkenShip(humanBoard);
            }
            if (isSunkShipOnComputerBoard) {
                removeSunkenShip(computerBoard);
            }

            //consoleUI.printGameBoard(computerBoard);
            consoleUI.printGameBoard(humanAuxBoard);
            consoleUI.printGameBoard(humanBoard);

            shots++;
            consoleUI.printGameReport(
                    shots,
                    isSuccessComputerShot,
                    humanBoard.getCoordinatesMap().getCoordinates(computerShot[0], computerShot[1]),
                    isSunkShipOnComputerBoard,
                    computerBoard.getNavy().size(),
                    isSuccessHumanShot,
                    humanBoard.getCoordinatesMap().getCoordinates(row, col),
                    isSunkShipOnHumanBoard,
                    humanBoard.getNavy().size()
            );

        }

        consoleUI.printResult(isHumanWinner());

    }

    private boolean isGameNotFinish(Board board) {

        return board.getNavy().size() != 0;
    }

    private boolean isHumanWinner() {

        return humanBoard.getNavy().size() != 0;

    }

    private boolean checkSunkenShip(Board board) {

        ArrayList<Ship> shipArrayList = board.getNavy();
        for (Ship ship : shipArrayList) {
            if (ship.isShipSunk(board)) {
                return true;
            }
        }

        return false;
    }

    private Ship getSunkenShip(Board board) {

        ArrayList<Ship> shipArrayList = board.getNavy();
        for (Ship ship : shipArrayList) {
            if (ship.isShipSunk(board)) {
                return ship;
            }
        }

        return null;
    }

    private void removeSunkenShip(Board board) {

        Ship sunkShip = getSunkenShip(board);

        if (sunkShip != null) {
            board.getNavy().remove(sunkShip);
        }
    }

    private boolean isSuccessHumanShot(int row, int col) {

        return computerBoard.isTileSuccessShot(row, col);
    }

    private boolean isSuccessComputerShot(int row, int col) {

        return humanBoard.isTileSuccessShot(row, col);

    }


}
