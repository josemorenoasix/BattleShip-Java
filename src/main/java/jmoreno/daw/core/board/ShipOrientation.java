package jmoreno.daw.core.board;

public enum ShipOrientation {
    VERTICAL, HORIZONTAL
}
