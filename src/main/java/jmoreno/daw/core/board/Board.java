package jmoreno.daw.core.board;


import java.util.ArrayList;

public class Board {

    private Tile[][] gameBoard;
    private int boardRows;
    private int boardCols;
    private ArrayList<Ship> navy;
    private CoordinatesMap coordinatesMap;


    /**
     * Constructor to set the size of the game board.
     * Fill the game board with WATER.
     *
     * @param rows Number of rows
     * @param cols Number of columns
     */
    public Board(int rows, int cols) {
        this.boardRows = rows;
        this.boardCols = cols;
        this.navy = new ArrayList<Ship>();
        this.coordinatesMap = new CoordinatesMap(rows, cols);

        this.gameBoard = new Tile[rows][cols];
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                this.gameBoard[r][c] = Tile.WATER;
            }
        }
    }

    public Tile getBoardTile(int row, int col) {

        return gameBoard[row][col];
    }

    public int getBoardRows() {
        return boardRows;
    }

    public int getBoardCols() {
        return boardCols;
    }

    public CoordinatesMap getCoordinatesMap() {
        return coordinatesMap;
    }

    public ArrayList<Ship> getNavy() {
        return navy;
    }

    public void addShip(Ship ship) {
        this.navy.add(ship);
    }

    public void markShip(int[][] position) {
        for (int i = 0; i < position.length; i++) {
            for (int j = 0; j < position[i].length; j++) {
                changeTileStatus(Tile.SHIP, position[i][0], position[i][1]);
            }
        }
    }


    public void shot(int row, int col) {

        switch (getBoardTile(row, col)) {
            case WATER:
                markFailShot(row, col);
                break;
            case SHIP:
                markSuccessShot(row, col);
                break;
            default:
                break;
        }

    }

    public void markSuccessShot(int row, int col) {

        changeTileStatus(Tile.SUCCESS_SHOT, row, col);

    }

    public void markFailShot(int row, int col) {

        changeTileStatus(Tile.FAIL_SHOT, row, col);
    }

    public boolean isTileWater(int row, int col) {
        return getBoardTile(row, col) == Tile.WATER;
    }

    public boolean isTileShip(int row, int col) {
        return getBoardTile(row, col) == Tile.SHIP;
    }

    public boolean isTileSuccessShot(int row, int col) {
        return getBoardTile(row, col) == Tile.SUCCESS_SHOT;
    }

    public boolean isTileFailShot(int row, int col) {
        return getBoardTile(row, col) == Tile.FAIL_SHOT;
    }

    private void changeTileStatus(Tile status, int row, int col) {
        this.gameBoard[row][col] = status;
    }

}
