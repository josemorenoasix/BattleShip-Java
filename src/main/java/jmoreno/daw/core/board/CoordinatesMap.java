package jmoreno.daw.core.board;

import java.util.HashMap;
import java.util.Map;

public class CoordinatesMap {

    private Map<Integer, Character> columnMap;
    private Map<Integer, Integer> rowMap;

    CoordinatesMap(int row, int col) {

        this.columnMap = new HashMap<Integer, Character>();
        this.rowMap = new HashMap<Integer, Integer>();

        for (int i = 0; i < col; i++) {
            this.columnMap.put(i, (char) (i + 65));
        }

        for (int i = 0; i < row; i++) {
            this.rowMap.put(i, i);
        }

    }

    public Integer getRow(String coordinatesString) {

        if (coordinatesString != null) {
            return getRow(Integer.parseInt(coordinatesString.substring(1)));
        }
        return null;

    }

    public Integer getCol(String coordinatesString) {

        if (coordinatesString != null) {
            return getCol(coordinatesString.charAt(0));
        }
        return null;

    }

    public String getCoordinates(int row, int col) {

        StringBuilder coordinates = new StringBuilder();

        for (Map.Entry<Integer, Character> entry : this.columnMap.entrySet()) {
            if (entry.getKey() == col) {
                coordinates.append(entry.getValue().toString());
            }
        }

        for (Map.Entry<Integer, Integer> entry : this.rowMap.entrySet()) {
            if (entry.getKey() == row) {
                coordinates.append(entry.getValue().toString());
            }
        }

        return coordinates.toString();

    }

    private Integer getRow(int intValue) {
        for (Map.Entry<Integer, Integer> entry : this.rowMap.entrySet()) {
            if (entry.getKey() == intValue) {
                return entry.getValue();
            }
        }
        return null;

    }

    private Integer getCol(char charValue) {
        for (Map.Entry<Integer, Character> entry : this.columnMap.entrySet()) {
            if (entry.getValue() == charValue) {
                return entry.getKey();
            }
        }
        return null;
    }

}
