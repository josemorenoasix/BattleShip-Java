package jmoreno.daw.core.board;


public class ShipPlacement {

    private Board board;
    private Ship ship;
    private int row;
    private int col;


    ShipPlacement(Board board, Ship ship, int row, int col) {
        this.board = board;
        this.ship = ship;
        this.row = row;
        this.col = col;
    }


    public boolean placeShip() {

        if (isValid()) {

            int[][] shipCoordinates = new int[this.ship.getSize()][2];

            for (int i = 0; i < this.ship.getSize(); i++) {
                switch (this.ship.getOrientation()) {
                    case HORIZONTAL:
                        shipCoordinates[i][0] = row;
                        shipCoordinates[i][1] = col + i;
                        break;
                    case VERTICAL:
                        shipCoordinates[i][0] = row + i;
                        shipCoordinates[i][1] = col;
                        break;
                }

            }

            this.ship.setPosition(shipCoordinates);
            return true;

        } else {

            return false;
        }
    }

    private boolean isValid() {

        return isInside() && isEnoughSpace();
    }

    private boolean isInside() {

        switch (this.ship.getOrientation()) {
            case VERTICAL:
                if ((row + this.ship.getSize()) > this.board.getBoardRows()) {
                    return false;
                }

                break;
            case HORIZONTAL:
                if ((col + this.ship.getSize()) > this.board.getBoardCols()) {
                    return false;
                }
                break;

        }

        return true;

    }

    private boolean isEnoughSpace() {

        try {
            if (board.isTileShip(row, col)) return false;
        } catch (Exception e) {
        }
        try {
            if (board.isTileShip(row - 1, col)) return false;
        } catch (Exception e) {
        }
        try {
            if (board.isTileShip(row - 1, col + 1)) return false;
        } catch (Exception e) {
        }
        try {
            if (board.isTileShip(row, col + 1)) return false;
        } catch (Exception e) {
        }
        try {
            if (board.isTileShip(row + 1, col + 1)) return false;
        } catch (Exception e) {
        }
        try {
            if (board.isTileShip(row + 1, col)) return false;
        } catch (Exception e) {
        }
        try {
            if (board.isTileShip(row + 1, col - 1)) return false;
        } catch (Exception e) {
        }
        try {
            if (board.isTileShip(row, col - 1)) return false;
        } catch (Exception e) {
        }
        try {
            if (board.isTileShip(row - 1, col - 1)) return false;
        } catch (Exception e) {
        }

        if (this.ship.getSize() > 1) {
            for (int i = 0; i < this.ship.getSize(); i++) {
                switch (this.ship.getOrientation()) {
                    case HORIZONTAL:
                        try {
                            if (board.isTileShip(row + 1, col + i)) return false;
                        } catch (Exception e) {
                        }
                        try {
                            if (board.isTileShip(row - 1, col + i)) return false;
                        } catch (Exception e) {
                        }
                        try {
                            if (board.isTileShip(row, col + i - 1)) return false;
                        } catch (Exception e) {
                        }
                        try {
                            if (board.isTileShip(row, col + i + 1)) return false;
                        } catch (Exception e) {
                        }
                        break;

                    case VERTICAL:
                        try {
                            if (board.isTileShip(row + i, col + 1)) return false;
                        } catch (Exception e) {
                        }
                        try {
                            if (board.isTileShip(row + i, col - 1)) return false;
                        } catch (Exception e) {
                        }
                        try {
                            if (board.isTileShip(row + i - 1, col)) return false;
                        } catch (Exception e) {
                        }
                        try {
                            if (board.isTileShip(row + i + 1, col)) return false;
                        } catch (Exception e) {
                        }
                        break;
                }
            }
        }

        return true;
    }


}
