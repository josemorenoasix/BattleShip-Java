package jmoreno.daw.core.board;


public class Ship {

    private int size;
    private int[][] position;
    private ShipOrientation orientation;

    Ship(int size, ShipOrientation orientation) {
        this.size = size;
        this.orientation = orientation;
    }

    public ShipOrientation getOrientation() {
        return orientation;
    }

    public void setOrientation(ShipOrientation value) {
        this.orientation = value;
    }

    public int[][] getPosition() {
        return this.position;
    }

    public void setPosition(int[][] value) {
        this.position = value;
    }

    public int getSize() {
        return this.size;
    }

    public boolean isShipSunk(Board board) {
        int touches = 0;

        for (int i = 0; i < size; i++) {
            if (board.isTileSuccessShot(position[i][0], position[i][1])) {
                touches++;
            }
        }
        return touches == size;
    }

}

