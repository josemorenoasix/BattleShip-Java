package jmoreno.daw.core.board;

import java.util.Random;

public class BoardFactory {

    private Random rnd;

    public BoardFactory() {
        this.rnd = new Random();

    }

    /**
     * Distribute ships on board randomly.
     *
     * @param board         boardgame of the player
     * @param shipArraySize integer array of ships' sizes.
     **/

    public void randomize(Board board, int[] shipArraySize) {

        int shipNumber = 0;
        int shipsCount = shipArraySize.length;

        while (shipsCount != 0) {
            Ship ship = new Ship(shipArraySize[shipNumber], randomShipOrientation());
            ShipPlacement shipPlacement;
            do {
                shipPlacement = new ShipPlacement(
                        board,
                        ship,
                        randomRow(board),
                        randomCol(board));

            } while (!shipPlacement.placeShip());
            shipsCount--;
            board.addShip(ship);
            board.markShip(ship.getPosition());
            shipNumber++;
        }
    }


    /**
     * Return a random ship orientation (VERTICAL/HORIZONTAL)
     *
     * @return ShipOrientation
     **/

    private ShipOrientation randomShipOrientation() {

        if (rnd.nextBoolean()) {

            return ShipOrientation.VERTICAL;

        } else {

            return ShipOrientation.HORIZONTAL;

        }

    }


    /**
     * Return a random row of a board
     *
     * @param board boardgame of the player
     * @return int
     **/

    private int randomRow(Board board) {

        return rnd.nextInt(board.getBoardRows());

    }

    /**
     * Return a random column of a board
     *
     * @param board board game of the player
     * @return int
     **/

    private int randomCol(Board board) {

        return rnd.nextInt(board.getBoardCols());

    }

}
